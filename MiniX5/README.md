# **Static noise - MiniX5**
My miniX5 is a reprensentation of static noise most of us know from old televisions. We called it myre-krig / ant-war in my house, because it looks like little ants running around. 
<br>
<br>
My program is pretty simple. I have learned by my past mistakes, and tried not to be too ambitious. I have also let myself be inspired more by others work for this assignment. I had no ideas for this minix, so I saw a lot af the coding train videos on noise, and there he showed how to make this static noise with random. So I have recreated that and then added a bouncing ball that also is random, but do have some rules. 
<br>
<br>
The ball cannot move past the canvas sides and if it touches these sides, it bounces back and changes color. The ball should resemble the bouncing DVD logo, we also know from back in the days with old televisions, so that is kind of my theme and inspiration. 
<br>
<br>
![alt text](pictureOfCode.png)
[See video of my code here](<minix5 video.mov>)
<br>
<br>
Please run my code [here](https://claydon-smith.gitlab.io/aesthetic-programming/MiniX5)
<br>
Please view my full repository [here](https://gitlab.com/claydon-smith/aesthetic-programming/-/tree/main/MiniX5)

### **References**
[Reference for p5.js libary](https://p5js.org/reference/)
[Reference for The Coding Train](https://www.youtube.com/watch?v=ikwNrFvnL3g&list=PLRqwX-V7Uu6bgPNQAdxQZpJuJCjeOr7VD&index=4)