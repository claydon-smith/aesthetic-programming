let ball;
let speedX, speedY;
let ballColor;

function setup() {
  createCanvas(800, 800);

  // when working on high pixel density displays, susch as Retina displays on MacBooks, you need to set the pixel scaling. The argument in this functions specifies the pixel density factor
  pixelDensity(1); // each pixel in the canvas corresponds to one physical pixel on the screen

  // creating a vector with a random x and y coordinate within the canvas bounds, placing the ball on a random position everytime the browser is refreshed 
  ball = createVector(random(width), random(height));

  // the speed of the ball. It will always move with a speed of 3 pixels per frame both horizontally and vertically 
  speedX = 3;
  speedY = 3;

  // creates a random color for the ball once the program starts
  ballColor = color(random(255), random(255), random(255));
}

function draw() {
  // the static noise background. I have made this with help from a coding train video, see refrences
  loadPixels(); // loads the current value of each pixel on the canvas into the pixels array

  // nested loop that sets each pixel on the canvas to a random grayscale color, with gives the effect of static noise
  for (let x = 0; x < width; x++) { // outer loop iterates the x-coordinate
    for (let y = 0; y < height; y++) { // inner loop iterates the y-coordinate
       // the index is calculated to access each pixel's color components in the pixels array
      let index = (x + y * width) * 4; // you multiply by 4 because each pixel occupies four elements: red, green, blue, alpha
      let r = random(255);
      pixels[index + 0] = r; // red value
      pixels[index + 1] = r; // green value
      pixels[index + 2] = r; // blue value
      pixels[index + 3] = 255; // alpha value, making the pixel fully visible/opaque
    }
  }
  updatePixels(); // updates the canvas and any changes made to the pixel data

  let ballSize = 40; 

  // draw the bouncing ball on the canvas
  fill(ballColor);
  ellipse(ball.x, ball.y, ballSize, ballSize);

  // make the ball move
  ball.x += speedX;
  ball.y += speedY;

  // if statement to check for collisions with the sides of the canvas and the ball
  if (ball.x - ballSize / 2 < 0 || ball.x + ballSize / 2 > width) {
    speedX *= -1; // change the direction
    ballColor = color(random(255), random(255), random(255)); // change the color of the ball to random color
  }
  if (ball.y - ballSize / 2 < 0 || ball.y + ballSize / 2 > height) {
    speedY *= -1; // change the direction
    ballColor = color(random(255), random(255), random(255)); // change the color of the ball to random color
  }
}