# **Throbber - MiniX3**
My third miniX is a site that lets you know when it is weekend. It is inspired by the site: er det fredag. The program consists of a headline, that tells what day of the week it is and an emoji, to expres the feeling of the day. Under the headline I've drawn a wine glass, that depending on the day, have more or less wine in it. Underneath that there is a loading bar, that goes from 0 to 100%, to show that we are loading for the weekend. I've challenge myself with this code, using both if statements, an array and a while loop. I also did a lot more research this time, but I still think it is difficult to know if I'm doing the right thing. 
<br>
<br>
If there was anything I wanted to make different, it is to make the loading bar stop an maybe turn green, when it is the weekend. 
<br>
<br>
![Screenshot of my code](throbber.png)
<br>
<br>
Please run my code [here](https://claydon-smith.gitlab.io/aesthetic-programming/MiniX3)
<br>
Please view my full repository [here](https://gitlab.com/claydon-smith/aesthetic-programming/-/tree/main/MiniX3)

## **Reflection**
I haven't made a traditional throbber. For me, a traditional throbber is a round spinning thing, but the diffenition, when googling it, it is a thing that resembles loading. I personally think that a lot of people would be happy or positive surprised, if they where met with my program as their loading screen. It is festive and fun. 
<br>
<br>
The reason why my throbber is a countdown for the weekend is, beacuse I feel that we all are waiting for the weekend. When you're at work, you say to your coworkers "there is only 2 days till it is weekend". I just feel that everybody is counting down, so now you have this program, to do it for you. You're welcome!

### **References**
[Reference for p5.js libary](https://p5js.org/reference/)
<br>
[Reference for function date()](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Date)
<br>
[Reference for er det fredag](https://erdetfredag.dk)