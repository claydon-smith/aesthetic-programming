// my x coordiante is the same many times, so I have defined this variable
let x = 400;

let weekdays;
let today;
let dayOfWeek;

function setup() {
  createCanvas(800, 800);
}

function draw() {
  background(245, 222, 179);
  // text at the top that tells the weekday
  // variables that defines the weekdays and get the current day
  weekdays = ['Sunday 😴', 'Monday 😭', 'Tuesday 😒', 'Wednesday 😫', 'Thursday 😐', 'Friday! 🥳', 'Saturday! 🤩'];
  today = new Date(); // functions are found in mdn web docs (see refrences)
  dayOfWeek = today.getDay();
  // text formating
  textAlign(CENTER, CENTER);
  textSize(30);
  textFont('Courier New');
  fill(0);
  text('Today is ' + weekdays[dayOfWeek], x, 200);

  // calling the function wineGlass, so you can see it on canvas
  wineGlass();
  
  // wine if statements to determine how much wine there is in the glass. Nothing in the begining of the week, but then more and more towards the weekend  
  if (dayOfWeek == [0]) {
    fill(139, 0, 0);
    arc(x, 320, 70, 140, 0, PI, CHORD);
  } else if (dayOfWeek == [2]) { // the reason why I skip 1, is that 1 is monday and monday is the default glass with no wine, so 1 is the else statement
    fill(139, 0, 0);
    arc(x, 370, 45, 50, 0, PI, CHORD);
  } else if (dayOfWeek == [3]) {
    fill(139, 0, 0);
    arc(x, 345, 60, 100, 0, PI, CHORD);
  } else if (dayOfWeek == [4]) {
    fill(139, 0, 0);
    arc(x, 320, 70, 140, 0, PI, CHORD);
  } else if (dayOfWeek == [5]) {
    fill(139, 0, 0);
    arc(x, 280, 90, 230, 0, PI, CHORD);
  } else if (dayOfWeek == [6]) {
    fill(139, 0, 0);
    arc(x, 265, 90, 265, 0, PI, CHORD);
  } else {
  }

  // calling the function loadingBar, so you can see it on canvas
  loadingBar();
}

function wineGlass() {
  // foot on the glass
  ellipseMode(CENTER);
  fill(255);
  ellipse(x, 520, 100, 30);

  // stem
  push(); // to get my rectMode (CENTER) to only apply to this part, I write push and pop, and this also do, that my loading bar loads from left to right
  rectMode(CENTER);
  rect(x, 440, 10, 150, 4);

  // bowl
  arc(x, 260, 100, 280, 0, PI, CHORD);
}

// variable to keep track of the progress of the loading bar
let progress = 0;

function loadingBar() {
  // outline of loading bar
  fill(255);
  rect(x, 600, 500, 40);
  pop();

  // blue fill / progress of loading bar
  fill(210, 180, 140);
  let progressWidth = map(progress, 0, 100, 0, 500);
  rect(150, 580, progressWidth, 40);

  // while loop to fill out the loading bar and starting over
  let counter = 0;
  while (counter < 1) { // adjust this number to control the speed
    if (progress < 100) {
      progress++;
    } else {
      progress = 0;
    }
    counter++;
  }

  // text that display the percentage
  textAlign(CENTER, CENTER);
  textSize(20);
  textFont('Courier New');
  fill(0);
  text('Weekend loading: ' + progress + '%', x, 600);
}