function setup() {
  createCanvas (800,800);
}

function draw() {
  background (255);

// text
  fill (0);
  strokeWeight (1);
  textAlign (CENTER);
  textFont ('Courier New',20);
  // top right corner
  text ('"Okay, I like it...',550,250);
  // bottom left corner
  text ('...Picasso"',250,550)

// variable to define the color of the emojies, I create this so I can call it instead of writing the colour code twice
  let emojiColour = "rgb(255,215,0)"; 

// thickness of stroke and lines i the rest of the code. So whenever I write stroke or line, it is this thicknes
  strokeWeight (6);


// emoji 1, top left corner
  fill (emojiColour);
  noStroke ();
  circle (250,250,200);

// shapes in different colours
  fill (0,0,255);
  quad(160,240,260,280,160,320,60,280);
  fill (255,0,0);
  square(260, 120, 140, 20, 15, 10, 5);

// adding fill and choosing the colour and removal of the stroke on the pupils and mouth
  fill (0);
  noStroke ();

// pupils
  // left
  circle (220,210,30);
  // right
  circle (330,210,30);
  
//mouth 
  ellipse (250,330,25,30);

// adding stroke and removing fill, and picking the colour of the eyes, eyebrows, nose and hair
  stroke (0);
  noFill ();

// eyes
  // left
  circle (220,210,60);
  // right
  circle (330,210,60);

// eyebrows
  // left
  line (180,160,240,140);
  // right
  line (300,130,360,150);

// nose
  // vertical
  line (285,230,285,290);
  // horizontal 
  line (270,290,285,290);

// hair
  // vertical
  line (380,110,380,300);
  // curve
  bezier (380,110,340,80,260,80,160,140);



// emoji 2, bottom right corner
  fill (emojiColour);
  noStroke ();
  circle (550,550,200);

// shapes in different colours
  fill (0,255,0);
  ellipse(510,480,120,70);
  fill (255,0,255);
  quad(580,500,520,620,580,680,640,620);

// eye
  fill (0);
  ellipse (520,510,15,30);

// colour of the ear, eyebrow, nose, mouth, chin and hair
  stroke (0);
  noFill ();

// ear
  ellipse (600,550,40,20);

// eyebrow
  line (480,480,540,480);

// nose
  line (490,540,450,570);
  line (450,570,480,580);

// mouth
  bezier (485,620,500,620,520,620,520,600);

// chin
  line (500,660,580,660);

// hair, to make the hair, I have created a lot of lines, to make som open triangels 
// the coordinats of Y are always the same to values, depending on if the Y coordinate is up or down. Therefore I've created two variables, that define y's coordinates. 
  let down = 450; 
  let up = 400;
  // from the left to the right
  line (450,down,480,up);
  line (480,up,510,down);
  line (510,down,540,up);
  line (540,up,570,down);
  line (570,down,600,up);
  line (600,up,630,down);

}