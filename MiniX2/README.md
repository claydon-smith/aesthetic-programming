# **Emoji - MiniX2**
My second miniX is two emojis inspired by picasso. I've worked with a lot of different shapes, but kept it pretty simple in terms of experimenting with other opportunities with p5.js. The nose, for instant, is create with two lines, to make it look like a triangle that is open, but I also know that you can do this with vertex, and beginShape / endShape, but I didn't quite get it. 
<br>
<br>
If I would recreate this assignment, I would look more into making the shapes smarter, maybe with more use of variables. It was a lot of work figuring out where the different coordinates need to be, and I'm sure you could have done it smarter. So more research next time. 
<br>
<br>
![Screenshot of my code](emojis.png)
<br>
<br>
Please run my code [here](https://claydon-smith.gitlab.io/aesthetic-programming/MiniX2)
<br>
Please view my full repository [here](https://gitlab.com/claydon-smith/aesthetic-programming/-/tree/main/MiniX2)

## **Reflection**
The assignment was to create two emojis that experiment with various geometric drawing methods and explores alternitives, particularly with regard to shapes and drawing with colours. You also needed to reflect on the politics/aesthetics of emojis. 
<br>
<br>
My take on emojis is, that it is really difficult to make emojis where everybody feels represented and nobody is forgotten. Also because we are so different and the emoji library would be way to big and hard to navigate. So, my solution is to make the emojis look less like people, to focus more on the emotions, than the looks of them. That is why I'm inspired by Picasso. His paintings are very abstract, but you're not in doubt that it is a human that is painted. So with the use of a lot of weird shapes and wild colours, I've created to emojis that expresses feelings and emotions, but don't represent anybody. 

### **References**
[Reference for p5.js libary](https://p5js.org/reference/)