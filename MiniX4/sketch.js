// variable to define what to show on the screen 
let show = "cookiesBox";
// variables to track visibility of each hacker box
let locationBoxVisible = true;
let contactInfoBoxVisible = true;
let cameraOnBoxVisible = true;
let audioOnBoxVisible = true;
let searchHistoryBoxVisible = true;
let fullAccessBoxVisible = true;
let picturesBoxVisible = true;

let videoInitialized = false;

function setup() { // the buttons apperently needs to be called in the setup function, to be able to remove them later on
  createCanvas(640, 480);
  buttonsCookiebox();
  activateLaserButton();
  laserButton.hide(); // this hides the button, so it is now shoen from the beginning 
}

function draw() {
  background(176, 196, 222);

  if (show === "cookiesBox") { // first I want the cookies box to show, so the first criteria is if show is equal to cookiesbox then it show cookies box, and it is this logic I use
    cookiesBox();
  } else if (show === "hackerBoxes") { // after that it checks if show is equal to hackeboxes, and show changes to hackerboxes once accept is pressed
    hackerBoxLocation();
    hackerBoxContactInfo();
    hackerBoxCameraOn();
    hackerBoxAudioOn();
    hackerBoxSearchHistory();
    hackerBoxFullAcces();
    hackerBoxPictures();
    acceptButton.remove(); // removes the accept button
    preferencesButton.remove(); // removes preferences button
    laserButton.show(); // makes the button visible
  } else if (show === "laserEyes") { // now it checks if show is equal to laser eyes. Once the button "eliminate your data" is pressed, show changes once again
    if (!videoCapture) {
      createVideoCapture();
      setupFaceTracker();
    }
    laserEyes();
    laserButton.remove(); // removes the laser button
  }
}

// function to check if mouse is over a specific hacker box
function isMouseOverBox(boxX, boxY, boxWidth, boxHeight) {
  return mouseX > boxX && mouseX < boxX + boxWidth && mouseY > boxY && mouseY < boxY + boxHeight;
}

function mouseMoved() {
  if (!videoInitialized) {
    return;
  }
  // check if mouse is over each hacker box and hide it if true
  if (isMouseOverBox(50, 50, 200, 80)) {
    locationBoxVisible = false;
  }
  if (isMouseOverBox(300, 270, 200, 80)) {
    contactInfoBoxVisible = false;
  }
  if (isMouseOverBox(400, 150, 200, 80)) {
    cameraOnBoxVisible = false;
  }
  if (isMouseOverBox(370, 20, 200, 80)) {
    audioOnBoxVisible = false;
  }
  if (isMouseOverBox(80, 180, 200, 80)) {
    searchHistoryBoxVisible = false;
  }
  if (isMouseOverBox(30, 350, 200, 80)) {
    fullAccessBoxVisible = false;
  }
  if (isMouseOverBox(390, 380, 200, 80)) {
    picturesBoxVisible = false;
  }
}

// function pop-up box cookies 
function cookiesBox() {
  // white square
  fill(255);
  stroke(0);
  rectMode(CENTER);
  rect(320, 240, 500, 280, 10);
  // x in right corner
  line(540, 120, 550, 130);
  line(550, 120, 540, 130);
  // text: headline
  fill(0);
  noStroke();
  textSize(30);
  textFont('Verdana');
  text('Cookies Settings', 100, 170);
  // text: description
  textSize(15);
  rectMode(CORNER);
  text('We use cookies and similar technologies to help personal- ize content, tailor and measure ads, and provide a better experience. By clicking accept, you agree to this, as outli- ned in our Cookie Policy.', 100, 200, 450);
  // create "Accept" button, with action when pressed
}

function buttonsCookiebox() {
  // create "Accept" button
  acceptButton = createButton('Accept');
  acceptButton.size(210, 50)
  acceptButton.position(100, 300);
  acceptButton.style('background', 'black');
  acceptButton.style('color', 'white');
  acceptButton.style('border', 'none');
  acceptButton.style('font-family', 'Verdana');
  acceptButton.style('font-size', '20px');
  acceptButton.style('cursor', 'pointer'); // make cursor to pointer on hover
  acceptButton.mousePressed(showHackerBoxes); // once pressed, show is equal to hackerboxes and the if statement changes
  // create "Preferences" button
  preferencesButton = createButton('Preferences');
  preferencesButton.size(210, 50);
  preferencesButton.position(330, 300);
  preferencesButton.style('border', 'solid');
  preferencesButton.style('background', 'white');
  preferencesButton.style('font-family', 'Verdana');
  preferencesButton.style('font-size', '20px');
}

// function to display the hackerBoxes when acceptbutton is pressed
function showHackerBoxes() {
  show = "hackerBoxes";
}

// function with layout and style of the databoxes. Variables are made to change the placement and text
let x = 0
let y = 0
let textBox = ' '
function styleDataBoxes() {
  fill(0);
  stroke(0, 255, 0);
  strokeWeight(3);
  rect(x, y, 200, 80, 5);
  fill(0, 255, 0);
  noStroke();
  textFont('Courier New', 13);
  rect(x, y, 200, 15);
  text(textBox, x + 20, y + 25, 160)
}

function hackerBoxLocation() {
  if (locationBoxVisible) {
    x = 50;
    y = 50;
    textBox = 'Location is now known and shared.';
    styleDataBoxes();
  }
}

function hackerBoxContactInfo() {
  if (contactInfoBoxVisible) {
    x = 300;
    y = 270;
    textBox = 'Contacts, messages and emails are downloaded.';
    styleDataBoxes();
  }
}

function hackerBoxCameraOn() {
  if (cameraOnBoxVisible) {
    x = 400;
    y = 150;
    textBox = 'Camera is now on, I can see you.';
    styleDataBoxes();
  }
}

function hackerBoxAudioOn() {
  if (audioOnBoxVisible) {
    x = 370;
    y = 20;
    textBox = 'Audio is now on and everything is being recorded.';
    styleDataBoxes();
  }
}

function hackerBoxSearchHistory() {
  if (searchHistoryBoxVisible) {
    x = 80;
    y = 180;
    textBox = 'Search history and visited websites are now known.';
    styleDataBoxes();
  }
}

function hackerBoxFullAcces() {
  if (fullAccessBoxVisible) {
    x = 30;
    y = 350;
    textBox = 'Full access over computer: achieved.';
    styleDataBoxes();
  }
}

function hackerBoxPictures() {
  if (picturesBoxVisible) {
    x = 390;
    y = 380;
    textBox = 'Granted access to all pictures and videos.';
    styleDataBoxes();
  }
}

// function to create button to eliminate your data. When pressed the function activateLaserEyes is called and button pressed is true, so now the camera and laser eyes are on
function activateLaserButton() {
  laserButton = createButton('Eliminate your data');
  laserButton.size(180, 70);
  laserButton.position(240, 200);
  laserButton.style('background', 'white');
  laserButton.style('color', 'black');
  laserButton.style('border', 'solid');
  laserButton.style('font-family', 'Verdana');
  laserButton.style('font-size', '15px');
  laserButton.style('cursor', 'pointer'); // make cursor to pointer on hover
  laserButton.mousePressed(activateLaserEyes); // once pressed, show changes to lasereyes and the if statement goes on
}

function activateLaserEyes() {
  show = "laserEyes";
}

// function to start video. This is taken from the book and class
let videoCapture;
function createVideoCapture() {
  videoCapture = createCapture(VIDEO);
  videoCapture.size(640, 480);
  videoCapture.hide();
  videoInitialized = true;
}

// function to set up the facetracker. This is taken from the book and class
let cameraTracker;
function setupFaceTracker() {
  cameraTracker = new clm.tracker();
  cameraTracker.init(pModel);
  cameraTracker.start(videoCapture.elt);
}

// function to create lasereyes. This is made on background of what we learned in class
function laserEyes() {
  let positions = cameraTracker.getCurrentPosition();
  image(videoCapture, 0, 0, 640, 480);

  if (positions.length) {
    beginShape();
    for (let i of [23, 63, 24, 64, 25, 65, 26, 66]) {
      let rightEyePosition = positions[i]
      // colour of eyes and lines. Vertex to fill out the eyes
      fill(255, 0, 0);
      vertex(rightEyePosition[0], rightEyePosition[1]);
      stroke(255, 0, 0);
      // lines to make it look like laser
      line(rightEyePosition[0], rightEyePosition[1], mouseX, mouseY);
    }
    endShape(CLOSE);

    beginShape();
    for (let i of [30, 68, 29, 67, 28, 70, 69]) {
      let leftEyePosition = positions[i]
      // vertex to fill out the eyes
      vertex(leftEyePosition[0], leftEyePosition[1]);
      // lines to make it look like laser
      line(leftEyePosition[0], leftEyePosition[1], mouseX, mouseY);
    }
    endShape(CLOSE);
  }

  // the hackerboxes needs to be called here, to be shown once the camera turns on
  hackerBoxLocation();
  hackerBoxContactInfo();
  hackerBoxCameraOn();
  hackerBoxAudioOn();
  hackerBoxSearchHistory();
  hackerBoxFullAcces();
  hackerBoxPictures();

  noCursor(); //removes the cursor when laserEyes are activated 
}