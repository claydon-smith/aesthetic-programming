# **Cookie eliminator - MiniX4**
This code was a pain in my ass. I challenged myself a bit too much, and I could not have finished this assignment without my amazing study group (shoutout to Lærke).
<br>
<br>
My miniX4 shows a cookies pop-up, where your only option is to press accept. Once accept is pressed you are hacked, and all your information is now know by the hacker. But luckyly a anti virus program is there to halp you and gives you the opportunity to eliminate your data. When the button is pressed, the camera is turned on and you are gifted laser eyes. You can now with the help of your mouse eliminate your data from the hacker's posession and you are safe once again.
<br>
<br>
My miniX4 is putting light on the fact of how easyly our data can be captured by the wrong people and how you should be a little more careful.
<br>
<br>
I have used the code we made with Germán in class, to use the camera/video and face tracking. I worked with a lot of variables and if statements to make something visible at some times and so on. I also ues for loops and a lot of functions to group all the elements. 
<br>
<br>
I still find coding very difficult and I don't know if my codes are too simple or way to difficult. I feel like I'm groping blindly.
<br>
<br>
![Screenshot of my code](cookiessettings.png)
![Screenshot of my code](hackermessages.png)
![Screenshot of my code](lasereyes.png)
<br>
<br>
Please run my code [here](https://claydon-smith.gitlab.io/aesthetic-programming/MiniX4)
<br>
Please view my full repository [here](https://gitlab.com/claydon-smith/aesthetic-programming/-/tree/main/MiniX4)

## **Reflection**

### **References**
[Reference for p5.js libary](https://p5js.org/reference/)