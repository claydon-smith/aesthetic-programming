// pipe class
class Pipe {
  constructor() {
    this.spacing = 175; // the vertical space between the top and bottom pipes
    this.top = random(height / 6, 3 / 4 * height); // the y-coordinate of the top of the pipe, determined randomly between one-sixth and three-fourths of the canvas height
    this.bottom = height - (this.top + this.spacing); // the calculated distance between the bottom of the top pipe and the bottom of the canvas
    this.x = width; // the initial x-coordinate of the pipe, set to the width of the canvas to ensure the pipe starts off-screen
    this.w = 80; // the width of the pipe
    this.speed = 6; // the horizontal speed at which the pipe moves across the screen
    this.highlight = false; // a boolean flag indicating whether the pipe should be highlighted (e.g., when it's hit by the bird)
    this.hit = false; // flag to track if the pipe has been hit by the bird
  }

  hits(bird) { // this method checks if the bird has collided with the pipe. It takes the bird object as an argument
    if (bird.y < this.top || bird.y > height - this.bottom) { // it returns true if the bird's coordinates overlap with the pipe's coordinates, indicating a collision
      if (bird.x > this.x && bird.x < this.x + this.w) {
        this.highlight = true; // if a collision occurs, the highlight property of the pipe is set to true to visually indicate the collision
        return true;
      }
    }
    this.highlight = false;
    return false;
  }

  show() { // this method is responsible for displaying the pipe on the canvas
    fill(0, 128, 0); // the color of the rectangles is determined based on the highlight property. If highlight is true, the color is red; otherwise, it's green
    if (this.highlight) {
      fill(255, 0, 0);
    }
    rect(this.x, 0, this.w, this.top); // tt draws two rectangles representing the top and bottom parts of the pipe using the rect() function
    rect(this.x, height - this.bottom, this.w, this.bottom);
  }

  update() { // this method updates the position of the pipe by decrementing its x-coordinate (this.x) based on its speed (this.speed)
    this.x -= this.speed;
  }

  offscreen() { // This method checks if the pipe has moved off-screen (to the left of the canvas). If the pipe's x-coordinate is less than negative its width (-this.w), indicating it's entirely off-screen, it returns true. Otherwise, it returns false
    return this.x < -this.w;
  }
}