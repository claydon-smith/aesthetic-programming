# Flappy Bird 2.0 - MiniX6
I have recreated the lovely game flappy bird. With help from our lovely friend Danny from the coding train, I have redcreated the simple and funny game. The game automatically starts, but the bird first starts to move once the spacebutton is pressed. Now the bird is moving down and once the spacebutton is pressed, the bird fly up. You have 3 lives, and each time you hit one of the pipes, you loose a live. If all 3 lives are lost: GAME OVER. In this code I have used classes and objects to create the game. I have made 3 classes: the gameover screen, the bird and the pipes. 
<br>
<br>
![alt text](flappybird.png)
![alt text](gameover.png)
<br>
<br>
Please run my code [here](https://claydon-smith.gitlab.io/aesthetic-programming/MiniX6)
<br>
Please view my full repository [here](https://gitlab.com/claydon-smith/aesthetic-programming/-/tree/main/MiniX6)

### **References**
[Reference for p5.js libary](https://p5js.org/reference/)
<br>
[Reference for the coding train video](https://www.youtube.com/watch?v=cXgA1d_E-jY)