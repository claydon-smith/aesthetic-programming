// global variables
let bird;
let pipes = []; // an array to store the pipe objects in the game
let lives = 3;
let gameOver = false; // a boolean variable to track whether the game is over or not
let backgroundImage;
let littleBird;

function preload() {
  // it loads external media files, in this case, the background image and the bird image
  backgroundImage = loadImage("background.png");
  littleBird = loadImage("littlebird.png");
}

function setup() {
  createCanvas(640, 480);
  bird = new Bird(); // it initializes the bird variable with a new instance of the bird class
  pipes.push(new Pipe()); // it adds the first pipe object to the pipes array
}

function draw() {
  background(backgroundImage);

  // it displays the number of lives remaining at the top-left corner of the canvas
  textSize(24);
  fill(255);
  noStroke();
  textAlign(LEFT);
  text("Lives: " + lives, 20, 30);

  if (!gameOver) { // if the game is not over
    for (let i = pipes.length - 1; i >= 0; i--) { // it updates and displays each pipe in the pipes array
      pipes[i].show();
      pipes[i].update();

      // it checks for collisions between the bird and pipes, and updates the game state accordingly
      if (pipes[i].hits(bird)) {
        if (!pipes[i].hit) { // check if the pipe has already been hit in this frame
          lives--;
          pipes[i].hit = true; // set the hit flag to true to prevent multiple decrement for the same pipe
          if (lives === 0) {
            gameOver = true;
          }
        }
        pipes[i].highlight = true;
      }

      if (pipes[i].offscreen()) {
        pipes.splice(i, 1);
      }
    }

    // it updates and displays the bird's position
    bird.update();
    bird.show();

    // it adds a new pipe to the pipes array every 75 frames
    if (frameCount % 75 == 0) {
      pipes.push(new Pipe());
    }
  } else { // if the game is over
    // it displays the game over screen with the message "You lost! Press SPACE to restart."
    let gameOverScreen = new GameOverScreen("You lost! Press SPACE to restart.");
    gameOverScreen.show();
  }
}

function keyPressed() {
  if (gameOver && key == ' ') { // if the game is over and the spacebar is pressed, it calls the restartGame() function to restart the game
    restartGame();
  } else if (key == ' ') { // if the spacebar is pressed and the game is not over, it makes the bird jump by calling the bird.up() method
    bird.up();
  }
}

function restartGame() { // this function resets the game state when called
  gameOver = false;
  lives = 3;
  pipes = [];
  bird = new Bird();
  pipes.push(new Pipe());
  // it sets gameOver to false, resets the number of lives to 3, clears the pipes array, and initializes a new bird object and a new pipe object
}