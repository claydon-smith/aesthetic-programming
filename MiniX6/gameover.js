// game over screen class
class GameOverScreen {
  constructor(message) {
    this.message = message;
  }

  show() {
    background(0, 160);
    textAlign(CENTER);
    fill(153, 0, 0);
    textSize(30);
    stroke(0);
    strokeWeight(3);
    text('GAME OVER', width / 2, height / 2);
    fill(255);
    textSize(20);
    text(this.message, width / 2, height / 2 + 30);
  }
}