// bird class
class Bird {
  constructor() {
    this.y = height / 2; // the initial y-coordinate of the bird, set to half of the canvas height to position the bird vertically centered
    this.x = 64; // the x-coordinate of the bird
    this.gravity = 0.7; // a constant representing the downward acceleration due to gravity. This value affects how fast the bird falls
    this.lift = -12; // the amount of upward velocity applied when the bird jumps
    this.velocity = 0; // the current vertical velocity of the bird, initially set to 0
    this.startedMoving = false; // flag to track if the bird has started moving
  }

  show() { // this method is responsible for displaying the bird on the canvas
    image(littleBird, this.x, this.y, 60, 40);
  }

  up() { // this method is called when the bird needs to jump
    if (!this.startedMoving) { // check if the bird has not started moving
      this.startedMoving = true; // set the flag to true
    }
    // reset velocity to give a consistent jump force
    this.velocity = this.lift;
  }

  update() { // this method updates the position of the bird based on its current velocity
    if (this.startedMoving) { // only update the position if the bird has started moving
      this.velocity += this.gravity; 
      this.y += this.velocity; // it updates the bird's y-coordinate (this.y) by adding its current velocity, causing it to move vertically
    }
  }
}