function setup() {
  createCanvas (800,800);
}

function draw() {  
// To get the colours to change when it's day and night, I define variables to use in the code. To make it easier i use HEX instead of RGB
  let grassColour = '#77AB59'
  let houseColour = '#BE9B7B'
  let doorColour = '#4B3832'
  let windowColour = '#FFF4E6'
  let sunMoonColour = '#FFD700'

  if (mouseX < 400){
  // Day
    background ('#BAE1FF');
    
    // Text 
    fill ('#000000')
    textFont ('Courier New',20);
    text ('Good morning', 80,300);

  }
  else if (mouseX > 400){
  // Night
    background ('#003258');
    grassColour = '#3B562C'
    houseColour = '#3F2E1F'
    doorColour = '#16110F'
    windowColour = '#222222'
    sunMoonColour = '#C0C0C0'

     // Text 
     fill ('#FFFFFF')
     textFont ('Courier New',20);
     text ('Good night', 600,300);
  }

// Sun and moon
  fill (sunMoonColour);
  noStroke ();
  ellipse (200,150,100);

// Grass
  rectMode (CENTER);
  fill (grassColour);
  rect (400,950,800);

// "Body" of the house
  rectMode (CENTER);
  fill (houseColour);
  rect (400,500,200);

// Door
  fill (doorColour);
  rect (440,530,60,140);

// Window
  fill (windowColour);
  rect (360,500,60,80);
  
// Roof (uses doorColour, because they have the same colour)
  fill (doorColour);
  triangle (280,420,520,420,400,300);
}