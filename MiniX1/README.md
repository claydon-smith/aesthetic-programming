# **Day 'n' Night - MiniX 1**

My first miniX is a drawing of a house made with different p5.js shapes and when you move the mouse across the drawing, you can make it nighttime, and back to daytime. I used the mouseX reference from the p5.js reference libary to make this happen. See the pictures down below.

![Screenshot of my code, house in daytime](day.png)
![Screenshot of my code, house in nighttime](night.png)
<br>
Please run my code [here](https://claydon-smith.gitlab.io/aesthetic-programming/MiniX1)
<br>
Please view my full repository [here](https://gitlab.com/claydon-smith/aesthetic-programming/-/tree/main/MiniX1)

## **Reflection**
How is the coding process different from, or similar to, reading and writing text?
For me, coding is very different from reading and writing "normal" text, because you always need to think locigcally. I hope it makes sense. 
You need to think about rules, order, syntax, coordinates and a lot more. Yes, you also need to think about some of these thinks when reading and writing, but it is just not the same. Coding does not feel like riding a bike (yet) but reading and writing does, so I really need to think a lot, when coding. Every monday and wednesday after our lectures in coding, my brain is completly fried. 

### **References**

[Reference for p5.js libary](https://p5js.org/reference/)