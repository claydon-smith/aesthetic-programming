# Fortune Cookie Good Advice - MiniX7(8)
This project is an interactive fortune cookie program which gives random “personalized” advice when the cookie jar has been pressed. When you start the program, you choose whether or not you want advice. If you are up for it, you get to “break” open a fortune cookie, and to reveal some words of wisdom! This all works thanks to an API of advice - some good, some questionable, but advice, nonetheless. If the user doesn’t want advice, it is possible to press the ‘Not today, but thanks!’ button, which then stops the program, with a noLoop().  

**How it works:**
- Run the program. 
- Choose whether to receive advice. 
- Click to break open the fortune cookie. 
- Receive and read the advice provided. 
<br>

**API integration:**
<br>
The project integrates with the Advice Slip API to provide random advice to the user. The API returns a JSON object containing a piece of advice, which is then displayed to the user when they choose to receive advice. 
<br>

**Our experience with JSON API:**
<br>
We decided to go with the Advice Slip API, because of the easy accessibility of the API, but also because of the documentation provided, it is simple and straight forward. The ideas for the MiniX were based on the API, and how it could be implemented. With this, the idea of a fortune cookie came to mind. We decided to go with the idea, even though the API does not provide fortunes, but general advice instead, since it still fits the overall idea and concept of the program we wanted to create. With this API we do not really change the data but show the API as the original text form without manipulating how it is displayed. We have some degree of control over how we utilize the API within our own programs or applications, but it is the provider, who has control over the API, including its functionality, availability, and terms of use. They dictate how users can interact with the API and may enforce limits or restrictions on usage. 
<br>

**If given more time:**
<br>
If we had more time, we would have fixed the place you need to click to get another advice.  
<br>
Please run our code [here](https://claydon-smith.gitlab.io/aesthetic-programming/MiniX8)
<br>
Please view our full repository [here](https://gitlab.com/claydon-smith/aesthetic-programming/-/tree/main/MiniX8)

### **References**
[Reference for image() in p5.js](https://p5js.org/reference/#/p5/image) 
<br>
[Reference for if-else in p5.js](https://p5js.org/reference/#/p5/if-else) 
<br>
[Reference for class in p5.js](https://p5js.org/reference/#/p5/class)
<br>
[Reference for imageMode() in p5.js](https://p5js.org/reference/#/p5/imageMode)
<br>
[Reference for text() in p5.js](https://p5js.org/reference/#/p5/text)
<br>
[Reference for textAllign() in p5.js](https://p5js.org/reference/#/p5/textAlign) 
<br>
[Reference for intheritance in p5.js](http://staging.p5js.org/examples/objects-inheritance.html) 
<br>
[Reference for loadJSON in p5.js](https://p5js.org/reference/#/p5/loadJSON) 
<br>
[Reference for preload() in p5.js](https://p5js.org/reference/#/p5/preload) 
<br>
[Reference for noLoop() in p5.js](https://p5js.org/reference/#/p5/noLoop) 
<br>
[Reference for our API](https://api.adviceslip.com/advice)  